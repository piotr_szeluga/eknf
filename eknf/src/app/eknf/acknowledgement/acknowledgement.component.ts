import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AckDialogData } from '../models/ack-dialog-model';
import { take } from 'rxjs/internal/operators/take';

/**
 * Acknowledgement component
 * 
 * Used to display acknowledgement before generating an invoice
 */
@Component({
  selector: 'app-acknowledgement',
  templateUrl: './acknowledgement.component.html',
  styleUrls: ['./acknowledgement.component.scss']
})
export class AcknowledgementComponent implements OnInit {

  /** 
  * @param {MatDialogRef<AcknowledgementComponent>}
  * @param {MAT_DIALOG_DATA}
  */
  constructor(public dialogRef: MatDialogRef<AcknowledgementComponent>, @Inject(MAT_DIALOG_DATA) public data: AckDialogData) { }

  /** 
   * Subscribes to backdropClick() - closes the matDialog when backdrop clicked
   */
  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().pipe(
      take(1)
    ).subscribe(
      res => {
        this.data.confirmation = false;
        this.dialogRef.close(this.data);
      }
    );
  }

  /**
   * Sets this.data.confirmation to true and closes dialog
   */
  okClicked() {
    this.data.confirmation = true;
    this.dialogRef.close(this.data);
  }

  /**
   * Sets this.data.confirmation to false and closes dialog
   */
  cancelClicked() {
    this.data.confirmation = false;
    this.dialogRef.close(this.data);
  }
}
