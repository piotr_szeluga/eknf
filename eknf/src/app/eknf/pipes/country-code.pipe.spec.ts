import { CountryCodePipe } from './country-code.pipe';

describe('CountryCodePipe', () => {
  it('create an instance', () => {
    const pipe = new CountryCodePipe();
    expect(pipe).toBeTruthy();
  });
  it('should translate pl to Polski', () => {
    const pipe = new CountryCodePipe();
    expect(pipe.transform('pl')).toEqual('Polski')
  });
  it('should translate !@#$1234Qpl to Other', () => {
    const pipe = new CountryCodePipe();
    expect(pipe.transform('!@#$1234Qpl')).toEqual('Other')
  });
  it('should translate "" to Other', () => {
    const pipe = new CountryCodePipe();
    expect(pipe.transform('')).toEqual('Other')
  });
  it('should translate PL to Polski', () => {
    const pipe = new CountryCodePipe();
    expect(pipe.transform('PL')).toEqual('Polski')
  });
  it('should translate eN to English', () => {
    const pipe = new CountryCodePipe();
    expect(pipe.transform('eN')).toEqual('English')
  });
});
