import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AckDialogData } from '../models/ack-dialog-model';
import { take } from 'rxjs/operators';

/**
 * Used to display warning before deleting cached URLs
 */
@Component({
  selector: 'app-delete-window',
  templateUrl: './delete-window.component.html',
  styleUrls: ['./delete-window.component.scss']
})
export class DeleteWindowComponent implements OnInit {

  constructor(
      public dialogRef: MatDialogRef<DeleteWindowComponent>,
      @Inject(MAT_DIALOG_DATA) public data: AckDialogData
  ) { }

  /**
   * Subscribes to backdropClick() - closes dialog if backdrop is clicked
   */
  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().pipe(
      take(1)
    ).subscribe(
      res => {
        this.data.confirmation = false;
        this.dialogRef.close(this.data);
      }
    );
  }

  /**
   * Sets this.data.confirmation to true then closes dialog
   */
  okClicked() {
    this.data.confirmation = true;
    this.dialogRef.close(this.data);
  }

  /**
   * Sets this.data.confirmation to false then closes dialog
   */
  cancelClicked() {
    this.data.confirmation = false;
    this.dialogRef.close(this.data);
  }

}
