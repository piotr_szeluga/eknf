import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteWindowComponent } from './delete-window.component';
import { AppModule } from 'src/app/app.module';

describe('DeleteWindowComponent', () => {
  let component: DeleteWindowComponent;
  let fixture: ComponentFixture<DeleteWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //it('should create', () => {
    //expect(component).toBeTruthy();
  //});
});
