import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Form, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { invoLangs } from '../consts/invoice-form-consts';
import { TranslateService } from '@ngx-translate/core';
import { EknfApiService } from '../services/eknf-api.service';
import { take, retry, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ErrorWindowComponent } from '../error-window/error-window.component';
import { AcknowledgementComponent } from '../acknowledgement/acknowledgement.component';
import { RecoveredInvoice } from '../models/recovered-invoice';
import { DeleteWindowComponent } from '../delete-window/delete-window.component';

/**
 * Used to display form and handle it's logic
 */
@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.scss']
})
export class InvoiceFormComponent implements OnInit, OnDestroy {
  invoiceLanguages: {vVal: string, code: string}[] = invoLangs;
  inForm: FormGroup;
  sizes: boolean[] = [true, false, false];
  formIsLoading$: Subscription;
  AckDialogRef: MatDialogRef<AcknowledgementComponent>;
  ErrDialogRef: MatDialogRef<ErrorWindowComponent>;
  DelDialogRef: MatDialogRef<DeleteWindowComponent>;
  confirmationFlag = false;
  recoverable = false;
  recovered: RecoveredInvoice[];
  generated = false;
  pdfURL;

  /**
   * Adds language codes to be used in translation service
   * 
   * Tries to set browser language if it's supported, otherwise sets pl
   * 
   * Subscribes to eknfService.isLoading to check if progress spinner should be displayed
   * @param fBuilder 
   * @param translate Service used for translations
   * @param eknfService Service used for communication with server
   * @param dialog 
   */
  constructor(
    private fBuilder: FormBuilder, public translate: TranslateService, public eknfService: EknfApiService, private dialog: MatDialog) {
    translate.addLangs(['en', 'pl', 'de', 'es', 'fr', 'it', 'ru', 'ua', 'hu', 'ja', 'zh', 'ko']);
    translate.setDefaultLang('en');
    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|de|es|fr|it|ru|ua|hu|ja|zh|ko/) ? browserLang : 'pl');
    this.formIsLoading$ = this.eknfService.isLoading.subscribe();
  }

  /**
   * Initializes inForm variable using fBuilder
   * 
   * Checks if 'recovery' variable is set in localStorage, if it is, sets recoverable to true, otherwise sets recoverable to false
   */
  ngOnInit() {
    this.inForm = this.fBuilder.group({
      invoLang: new FormControl('', {updateOn: 'change', validators: []}),
      tickets: this.fBuilder.array([this.initTicket()]),
      name: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      country: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      city: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      zipcode: new FormControl('', {updateOn: 'change', validators: [Validators.required, Validators.maxLength(12), Validators.minLength(3)]}),
      street: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      bnumber: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      taxid: new FormControl('', {updateOn: 'change', validators: [Validators.required]}),
      phone: new FormControl('', {updateOn: 'change', validators: []}),
      email: new FormControl('', {updateOn: 'change', validators: [Validators.email, Validators.required]}),
    });
    localStorage.getItem('recovery') == null ? this.recoverable = false : this.recoverable = true;
  }

  /**
   * Sets all elements of sizes to false then sets element of index [val] to true
   * @param val Index of sizes to be set true
   */
  setSizes(val: number) {
    this.sizes = [false, false, false];
    this.sizes[val] = true;
  }
  /**
   * Saves parsed JSON from localStorage in recovered variable
   */
  recover() {
    this.recovered = JSON.parse(localStorage.getItem('recovery'));
  }
  /**
   * Pushes invoice data (URL, date of creation, first related ticket number)
   * @param itm Invoice data to be stored
   */
  push2recovery(itm: RecoveredInvoice) {
    const tmpStr = localStorage.getItem('recovery');
    let tmpArr = [];
    if (tmpStr !== null) {
      tmpArr = JSON.parse(tmpStr);
    }
    tmpArr.push(itm);
    localStorage.setItem('recovery', JSON.stringify(tmpArr));
  }
  /**
   * Removes recovery from localStorage
   */
  dropRecovery() {
    localStorage.removeItem('recovery');
    this.recoverable = false;
  }
  /**
   * @returns New form control for ticket number input
   */
  initTicket() {
    return this.fBuilder.control('', {updateOn: 'change', validators: [Validators.required, Validators.maxLength(10), Validators.minLength(10)]});
  }
  /**
   * Pushes fresh form control (ticket number) to formArray
   */
  addTicket() {
    const control = this.inForm.controls.tickets as FormArray;
    control.push(this.initTicket());
  }

  /**
   * Removes control from formArray (tickets)
   * @param id Index of control to be removed
   */
  removeTicket(id: number) {
    const control = this.inForm.controls.tickets as FormArray;
    control.removeAt(id);
  }
  /**
   * If formArray has status VALID triggers addTickets method
   */
  plusClicked() {
    if (this.inForm.controls.tickets.status === 'VALID') {
      this.addTicket();
    }
  }
  /**
   * Removes ticket field of index id
   * @param id Index of ticket field to be removed
   */
  minusClicked(id: number) {
    this.removeTicket(id);
  }

  /**
   * Forms XML body for http request
   * @param formData Object containing user inputs needed to generate an invoice
   */
  requestBuilder(formData: FormGroup): string {
    const tkts = formData.get('tickets') as FormArray;
    let out = `<invoice_request>
      <customer>
        <name>${formData.controls.name.value}</name>
        <address_line1>${formData.controls.street.value},${formData.controls.bnumber.value}</address_line1>
        <address_line2>${formData.controls.zipcode.value},${formData.controls.city.value},${formData.controls.country.value}</address_line2>
        <email>${formData.controls.email.value}</email>
      </customer>
      <form_of_payment/>
      <language>${formData.controls.invoLang.value}</language>
      <products>`;
    for (let i = 0; i < tkts.length ; i++) {
      out += `<product>
            <type>A</type>
            <number>${tkts.controls[i].value}</number>
          </product>`;
    }
    out += `</products>
    </invoice_request>`;

    return out;
  }

  /**
   * Uses eknf service to send request formed with requestBuilder then handles the response
   * @param formData Object containing user inputs needed to generate an invoice
   */
  submitConfirmed(formData: FormGroup) {
    this.eknfService.isLoading.next(true);
    localStorage.removeItem('pdfURL');
    this.eknfService.sendInvoiceData(this.requestBuilder(formData)).pipe(
      retry(2),
      take(1)
    ).subscribe(
      (res) => {
        const tkts = formData.get('tickets') as FormArray;
        this.eknfService.isLoading.next(false);
        const jres = this.eknfService.parseXMLresponse(res);
        if (jres.invoice_response.status.match(/OK/i) && jres.invoice_response.hasOwnProperty('pdf_url_tkt')) {
          this.push2recovery({ticketNo: tkts.controls[0].value , url: jres.invoice_response.pdf_url_tkt, date: new Date()});
          this.pdfURL = jres.invoice_response.pdf_url_tkt;
          this.generated = true;
        } else {
          this.ErrDialogRef = this.dialog.open(ErrorWindowComponent, {width: '80vw', maxWidth: '550px', data: {
            status: 200,
            message: jres.invoice_response.status
          }});
        }
      },
      (err) => {
        this.eknfService.isLoading.next(false);
        this.ErrDialogRef = this.dialog.open(ErrorWindowComponent, {width: '80vw', maxWidth: '550px', data: err});
      }
    );
  }
  /**
   * Displays confirmation window, if confirmed triggers submitConfirmed method
   * @param formData Object containing user inputs needed to generate an invoice
   */
  submitConfirm(formData: FormGroup) {
    this.AckDialogRef = this.dialog.open(AcknowledgementComponent, {width: '80vw', maxWidth: '550px', data: {confirmation: this.confirmationFlag}});
    this.AckDialogRef.afterClosed()
      .pipe(
        take(1)
      )
        .subscribe(
          (result) => {
            if (result.confirmation === true) {
              this.submitConfirmed(formData);
            }
          },
          (err) => {
            this.confirmationFlag = false;
          }
        );
  }
  /**
   * If form has status valid triggers submitConfirm method
   * @param formData Object containing user inputs needed to generate an invoice
   */
  submitClicked(formData: FormGroup) {
    if (formData.status === 'VALID') {
      this.submitConfirm(formData);
    }
  }
  /**
   * Displays confirmation window before deleting cached data
   */
  confirmCacheDelete() {
    this.DelDialogRef = this.dialog.open(DeleteWindowComponent, {width: '80vw', maxWidth: '550px', data: {confirmation: this.confirmationFlag}});
    this.DelDialogRef.afterClosed()
      .pipe(
        take(1)
      )
        .subscribe(
          (result) => {
            if (result.confirmation === true) {
              this.dropRecovery();
            }
          }
        );

  }
  /**
   * Unsubscribes from eknfServocr.isLoading
   */
  ngOnDestroy() {
    this.formIsLoading$.unsubscribe();
  }
}
