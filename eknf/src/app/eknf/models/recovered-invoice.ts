export interface RecoveredInvoice {
    ticketNo: number | string;
    url: string;
    date: Date | string | number;
}
