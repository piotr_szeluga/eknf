import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RecoveredInvoice } from '../models/recovered-invoice';
import { TranslateService } from '@ngx-translate/core';

/**
 * Displays list of records with invoice urls
 */
@Component({
  selector: 'app-recover',
  templateUrl: './recover.component.html',
  styleUrls: ['./recover.component.scss']
})
export class RecoverComponent implements OnInit {
  invoices: RecoveredInvoice[];
  @Output() clearInvoCache = new EventEmitter<boolean>();

  constructor(
    public translate: TranslateService
  ) { }

  /**
   * Saves data stored in localStorage in invoices variable
   */
  ngOnInit() {
    this.invoices = JSON.parse(localStorage.getItem('recovery'));
  }
  /**
   * Emits event to delete 'recovery' from local storage
   */
  delBtnClicked() {
    this.clearInvoCache.emit(true);
  }
}
