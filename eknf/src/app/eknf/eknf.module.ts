import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EknfApiService } from './services/eknf-api.service';
import { ReactiveFormsModule } from '@angular/forms';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { TranslateModule } from '@ngx-translate/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CountryCodePipe } from './pipes/country-code.pipe';
import { ErrorWindowComponent } from './error-window/error-window.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AcknowledgementComponent } from './acknowledgement/acknowledgement.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { RecoverComponent } from './recover/recover.component';
import { MatCardModule } from '@angular/material/card';
import { DeleteWindowComponent } from './delete-window/delete-window.component';



@NgModule({
  declarations: [
    InvoiceFormComponent,
    CountryCodePipe,
    ErrorWindowComponent,
    AcknowledgementComponent,
    RecoverComponent,
    DeleteWindowComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSelectModule,
    MatOptionModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    TranslateModule.forChild(),
    MatProgressSpinnerModule,
    MatDialogModule,
    MatExpansionModule,
    MatCardModule
  ],
  exports: [
    InvoiceFormComponent
  ],
  providers: [
    EknfApiService
  ],
  entryComponents: [
    ErrorWindowComponent, AcknowledgementComponent, DeleteWindowComponent
  ]
})
export class EknfModule { }
