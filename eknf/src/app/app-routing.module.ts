import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceFormComponent } from './eknf/invoice-form/invoice-form.component';


const routes: Routes = [
  {path : '', component : InvoiceFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
